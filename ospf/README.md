# Scripts pour le déploiement d'un OSPF sur GNS3

![Maquette OSPF](./maquette_ospf.png)

## Variables

Pour fonctionner correctement les variables `ip_serv` et `list_port` des fichiers `clients.exp`, `router_lan.exp`, `routeur_ospf.exp` doivent correspondre au valeurs fournies par GNS3.

* `ip_serv` : IP du serveur hébergeant les équipements
* `list_port` : Ports permettant de se connecter aux équipements via le protocol telnet

## Fonctionnement

Pour utiliser ces scripts vou devez avoir [expect](https://core.tcl-lang.org/expect/index) installé sur votre machine ensuite chaque script peut être lancé manuellement.

Exemple :

```bash
$ ./router_ospf.exp
```

Il est également possible de lancer tous les scripts de déploiement via un script bash :

```bash
$ ./deploy.sh
```