#!/usr/bin/expect

set ip_serv 192.168.226.1

# L3-M1
spawn telnet $ip_serv 5021
send "\r"
send "\r"
expect "#"
send "conf t\r"
expect "(config)#"
# Renommer les hotes
send "hostname L3-Master1\r"
expect "(config)#"
# Protocol PAGP
send "interface range fa1/0-1\r"
expect "(config-if-range)#"
send "channel-group 1 mode auto\r"
expect "(config-if-range)#"
send "exit\r"
expect "(config)#"
# Trunk
send "interface channel-group 1\r"
expect "(config-if)#"
send "switchport trunk encapsulation dot1q\r"
expect "(config-if)#"
send "switchport mode trunk\r"
expect "(config-if)#"
send "exit\r"
expect "(config)#"
# Ip des vlan
# vlan 1
send "interface vlan1\r"
expect "(config-if)#"
send "ip address 10.1.0.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 10
send "interface vlan10\r"
expect "(config-if)#"
send "ip address 10.1.10.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 20
send "interface vlan20\r"
expect "(config-if)#"
send "ip address 10.1.20.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 30
send "interface vlan30\r"
expect "(config-if)#"
send "ip address 10.1.30.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 40
send "interface vlan40\r"
expect "(config-if)#"
send "ip address 10.1.40.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 50
send "interface vlan50\r"
expect "(config-if)#"
send "ip address 10.1.50.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 60
send "interface vlan60\r"
expect "(config-if)#"
send "ip address 10.1.60.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# Protocol glbp


# L3-S1
spawn telnet $ip_serv 5022
send "\r"
send "\r"
expect "#"
send "conf t\r"
expect "(config)#"
# Renommer les hotes
send "hostname L3-Slave1\r"
expect "(config)#"
# Protocol PAGP
send "interface range fa1/0-1\r"
expect "(config-if-range)#"
send "channel-group 1 mode desirable\r"
expect "(config-if-range)#"
send "exit\r"
expect "(config)#"
# Trunk
send "interface channel-group 1\r"
expect "(config-if)#"
send "switchport trunk encapsulation dot1q\r"
expect "(config-if)#"
send "switchport mode trunk\r"
expect "(config-if)#"
send "exit\r"
expect "(config)#"
# Ip des vlan
# vlan 1
send "interface vlan1\r"
expect "(config-if)#"
send "ip address 10.1.0.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 10
send "interface vlan10\r"
expect "(config-if)#"
send "ip address 10.1.10.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 20
send "interface vlan20\r"
expect "(config-if)#"
send "ip address 10.1.20.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 30
send "interface vlan30\r"
expect "(config-if)#"
send "ip address 10.1.30.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 40
send "interface vlan40\r"
expect "(config-if)#"
send "ip address 10.1.40.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 50
send "interface vlan50\r"
expect "(config-if)#"
send "ip address 10.1.50.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 60
send "interface vlan60\r"
expect "(config-if)#"
send "ip address 10.1.60.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# Protocol glbp


# L3-M2
spawn telnet $ip_serv 5027
send "\r"
send "\r"
expect "#"
send "conf t\r"
expect "(config)#"
# Renommer les hotes
send "hostname L3-Master2"
expect "(config)#"
#  Protocol PAGP
send "interface range fa1/0-1\r"
expect "(config-if-range)#"
send "channel-group 2 mode auto\r"
expect "(config-if-range)#"
send "exit\r"
expect "(config)#"
#  Trunk
send "interface channel-group 2\r"
expect "(config-if)#"
send "switchport trunk encapsulation dot1q\r"
expect "(config-if)#"
send "switchport mode trunk\r"
expect "(config-if)#"
send "exit\r"
expect "(config)#"
# Ip des vlan
# vlan 1
send "interface vlan1\r"
expect "(config-if)#"
send "ip address 10.2.0.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 10
send "interface vlan10\r"
expect "(config-if)#"
send "ip address 10.2.10.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 20
send "interface vlan20\r"
expect "(config-if)#"
send "ip address 10.2.20.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 30
send "interface vlan30\r"
expect "(config-if)#"
send "ip address 10.2.30.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 40
send "interface vlan40\r"
expect "(config-if)#"
send "ip address 10.2.40.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 50
send "interface vlan50\r"
expect "(config-if)#"
send "ip address 10.2.50.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 60
send "interface vlan60\r"
expect "(config-if)#"
send "ip address 10.2.60.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# Protocol glbp


# L3-S2
spawn telnet $ip_serv 5029
send "\r"
send "\r"
expect "#"
send "conf t\r"
expect "(config)#"
# Renommer les hotes
send "hostname L3-Slave2"
expect "(config)#"
# Protocol PAGP
send "interface range fa1/0-1\r"
expect "(config-if-range)#"
send "channel-group 2 mode desirable\r"
# Trunk
send "interface channel-group 2\r"
expect "(config-if)#"
send "switchport trunk encapsulation dot1q\r"
expect "(config-if)#"
send "switchport mode trunk\r"
expect "(config-if)#"
send "exit\r"
expect "(config)#"
# Ip des vlan
# vlan 1
send "interface vlan1\r"
expect "(config-if)#"
send "ip address 10.2.0.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 10
send "interface vlan10\r"
expect "(config-if)#"
send "ip address 10.2.10.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 20
send "interface vlan20\r"
expect "(config-if)#"
send "ip address 10.2.20.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 30
send "interface vlan30\r"
expect "(config-if)#"
send "ip address 10.2.30.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 40
send "interface vlan40\r"
expect "(config-if)#"
send "ip address 10.2.40.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 50
send "interface vlan50\r"
expect "(config-if)#"
send "ip address 10.2.50.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 60
send "interface vlan60\r"
expect "(config-if)#"
send "ip address 10.2.60.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# Protocol glbp


# L3-M3
spawn telnet $ip_serv 5030
send "\r"
send "\r"
expect "#"
send "conf t\r"
expect "(config)#"
# Renommer les hotes
send "hostname L3-Master3"
expect "(config)#"
# Protocol PAGP
send "interface range fa1/0-1\r"
expect "(config-if-range)#"
send "channel-group 3 mode auto\r"
expect "(config-if-range)#"
send "exit\r"
expect "(config)#"
#  Trunk
send "interface channel-group 3\r"
expect "(config-if)#"
send "switchport trunk encapsulation dot1q\r"
expect "(config-if)#"
send "switchport mode trunk\r"
expect "(config-if)#"
send "exit\r"
expect "(config)#"
# Ip des vlan
# vlan 1
send "interface vlan1\r"
expect "(config-if)#"
send "ip address 10.3.0.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 10
send "interface vlan10\r"
expect "(config-if)#"
send "ip address 10.3.10.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 20
send "interface vlan20\r"
expect "(config-if)#"
send "ip address 10.3.20.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 30
send "interface vlan30\r"
expect "(config-if)#"
send "ip address 10.3.30.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 40
send "interface vlan40\r"
expect "(config-if)#"
send "ip address 10.3.40.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 50
send "interface vlan50\r"
expect "(config-if)#"
send "ip address 10.3.50.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 60
send "interface vlan60\r"
expect "(config-if)#"
send "ip address 10.3.60.1\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# Protocol glbp


# L3-S3
spawn telnet $ip_serv 5031
send "\r"
send "\r"
expect "#"
send "conf t\r"
expect "(config)#"
# Renommer les hotes
send "hostname L3-Slave3"
expect "(config)#"
# Protocol PAGP
send "interface range fa1/0-1\r"
expect "(config-if-range)#"
send "channel-group 3 mode desirable\r"
expect "(config-if-range)#"
send "exit\r"
expect "(config)#"
# Trunk
send "interface channel-group 3\r"
expect "(config-if)#"
send "switchport trunk encapsulation dot1q\r"
expect "(config-if)#"
send "switchport mode trunk\r"
expect "(config-if)#"
send "exit\r"
expect "(config)#"
# Ip des vlan
# vlan 1
send "interface vlan1\r"
expect "(config-if)#"
send "ip address 10.3.0.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 10
send "interface vlan10\r"
expect "(config-if)#"
send "ip address 10.3.10.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 20
send "interface vlan20\r"
expect "(config-if)#"
send "ip address 10.3.20.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 30
send "interface vlan30\r"
expect "(config-if)#"
send "ip address 10.3.30.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 40
send "interface vlan40\r"
expect "(config-if)#"
send "ip address 10.3.40.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 50
send "interface vlan50\r"
expect "(config-if)#"
send "ip address 10.3.50.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# vlan 60
send "interface vlan60\r"
expect "(config-if)#"
send "ip address 10.3.60.2\r"
expect "(confog-if)#"
send "no shutdown\r"
expect "(config-if)"
send "exit\r"
expect "(config)#"
# Protocol glbp