# Projet Bibi&Co

- [Projet Bibi&Co](#projet-bibico)
  - [Wan](#wan)

## Wan

![Backbone](Img/backbone.png)

Le réseau Wan est composé de 4 routeurs, Wan-(0-3). Wan-0 est relié à un réseau `NAT` et les autres à un site.

[Fichiers de configuration](Conf/)
